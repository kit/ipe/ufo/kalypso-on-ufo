## KALYPSO data analysis with UFO

This repository contains additional filters and a script to analyse data
acquired with KALYPSO, i.e.

    kalypso --background b.bin --modulated m.bin --unmodulated u.bin \
            --output corrected.tif

Run

    kalypso --help

to get a full list of options


### Installation

First install the development library (libufo-dev) or [build][] UFO from source.
Then create a build directory, run CMake and install the KALYPSO plugins and
driver binaries:

    $ mkdir build && cd build && cmake .. && make install

[build]: http://ufo-core.readthedocs.io/en/latest/install/linux.html#installing-from-source
