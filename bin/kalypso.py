#!/usr/bin/python

import argparse
import matplotlib.pyplot as plt
import numpy as np
import tifffile
import os
import logging
from ufo import Write


def correction(args):
    from ufo import Read, AverageKalypso, CorrectKalypso

    common = dict(type='raw', raw_width=256, raw_bitdepth=16, number=1, convert=False)

    bg = Read(path=args.background, raw_height=args.number, **common)
    unmod = Read(path=args.unmodulated, raw_height=args.number, **common)
    mod = Read(path=args.modulated, raw_height=args.number, **common)

    avg_bg = AverageKalypso()
    avg_unmod = AverageKalypso()
    correct = CorrectKalypso()

    return correct(mod(), avg_bg(bg()), avg_unmod(unmod()))


def low_pass(input_chain, args):
    from ufo import MovingAverage

    if not args.low_pass:
        return input_chain

    logging.info("Use lowpass filter")
    low_pass_filter = MovingAverage()
    return low_pass_filter(input_chain)


def arg_max(input_chain, args):
    from ufo import Argmax

    if not args.argmax:
        return input_chain

    logging.info("Use argmax filter")
    arg_max_filter = Argmax()
    return arg_max_filter(input_chain)


def region(input_chain, args):
    from ufo import Crop

    if not args.region:
        return input_chain

    logging.info("Use crop filter")
    start, stop = args.region
    crop = Crop(x=start, width=stop - start)
    return crop(input_chain)


def powerspectrum(input_chain, args):
    from ufo import Fft, Powerspectrum

    if not args.powerspectrum:
        return input_chain

    logging.info("Use fft and powerspectrum filter")
    fft = Fft()
    ps = Powerspectrum()
    return ps(fft(input_chain))


def time(input_chain, args):
    from ufo import Transpose

    if not args.time:
        return input_chain

    logging.info("Use transpose filter")
    transpose = Transpose()
    return transpose(input_chain)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    def is_valid_file(arg):
        if not os.path.exists(arg):
            parser.error("`{}' does not exist!".format(arg))
        else:
            return arg

    def is_valid_region(arg):
        try:
            s = arg.split(',')
            start, stop = int(s[0]), int(s[1])
            assert stop > start
            return start, stop
        except:
            parser.error("`{}' is not a region. Format is `start,stop', i.e.  `256,512'".format(arg))

    parser.add_argument('-b', '--background', required=True, metavar="FILE",
                        type=lambda x: is_valid_file(x),
                        help="File with background data")
    parser.add_argument('-m', '--modulated', required=True, metavar="FILE",
                        type=lambda x: is_valid_file(x),
                        help="File with modulated data")
    parser.add_argument('-u', '--unmodulated', required=True, metavar="FILE",
                        type=lambda x: is_valid_file(x),
                        help="File with unmodulated data")
    parser.add_argument('-o', '--output', required=True,
                        help="Output destination")
    parser.add_argument('-n', '--number', default=2048, type=int,
                        help="Number of data sets to analyze")
    parser.add_argument('--low-pass', action='store_true', default=False,
                        help="Low pass filtering using moving average")
    parser.add_argument('--argmax', action='store_true', default=False,
                        help="Determine argmax")
    parser.add_argument('--region', type=lambda x: is_valid_region(x), default=None,
                        help="Region of interest given as `start,stop'")
    parser.add_argument('--plot', type=int,
                        help="Plot resulting data on the given row")
    parser.add_argument('--powerspectrum', action='store_true',
                        help="Compute power spectrum along time axis")
    parser.add_argument('--time', action='store_true',
                        help="Compute in time domain")
    parser.add_argument('--profile', action='store_true', default=False,
                        help="Output OpenCL profile data")
    parser.add_argument('-v', '--verbosity', action='count', default=0,
                        help="Control verbosity of output")
    parser.add_argument('--version', action='version', version='%(prog)s 0.1')

    args = parser.parse_args()

    logging.basicConfig(level=max(logging.DEBUG, logging.WARNING - 10 * args.verbosity))
    funcs = [region, low_pass, time, powerspectrum, arg_max]
    processed = correction(args)

    for func in funcs:
        processed = func(processed, args)

    write = Write(filename=args.output)
    write(processed()).run(enable_tracing=args.profile).join()

    if args.plot:
        data = tifffile.imread(args.output)
        plt.plot(data[args.plot, :])
        plt.show(True)
