#include <string.h>
#include <ufo/ufo.h>

typedef struct {
    char *background;
    char *unmodulated;
    char *modulated;
    char *output;
    int number;
    gboolean profile;
    gboolean lowpass;
    gboolean argmax;
    gboolean time;
    gboolean powerspectrum;
    gboolean use_dgma;
    gboolean use_dummy;
} Parameters;

static UfoTaskNode *
make_node (UfoPluginManager *pm, const char *name)
{
    UfoTaskNode *node;
    GError *error = NULL;

    node = ufo_plugin_manager_get_task (pm, name, &error);

    if (error != NULL) {
        g_print ("Error loading plugin: %s.\n", error->message);
        return NULL;
    }

    return node;
}

static gboolean
get_enum_value (GObject *object, const gchar *prop_name, const gchar *name, gint *val)
{
    GParamSpec *pspec;
    GEnumClass *enum_class;

    pspec = g_object_class_find_property (G_OBJECT_GET_CLASS (object), prop_name);
    g_assert (G_IS_PARAM_SPEC (pspec));
    enum_class = G_PARAM_SPEC_ENUM (pspec)->enum_class;

    for (guint i = 0; i < enum_class->n_values; i++) {
        if (g_strcmp0 (enum_class->values[i].value_nick, name) == 0) {
            *val = enum_class->values[i].value;
            return TRUE;
        }
    }

    return FALSE;
}

static void
set_read_properties (UfoTaskNode *node, char *path, int height)
{
    gint val;

    g_assert (get_enum_value (G_OBJECT (node), "type", "raw", &val));
    g_object_set (node,
        "type", val, "path", path,
        "raw-width", 256, "raw-height", height, "raw-bitdepth", 16,
        "number", 1, "convert", FALSE,
        NULL);
}

static gboolean
run (Parameters *params)
{
    UfoPluginManager *pm;
    UfoTaskNode *bg;
    UfoTaskNode *avg_bg;
    UfoTaskNode *mod;
    UfoTaskNode *unmod;
    UfoTaskNode *avg_unmod;
    UfoTaskNode *correct;
    UfoTaskNode *write;
    UfoTaskNode *previous;
    UfoTaskGraph *graph;
    UfoBaseScheduler *sched;
    gdouble run_time;
    UfoTaskNode *lowpass = NULL;
    UfoTaskNode *transpose = NULL;
    UfoTaskNode *argmax = NULL;
    UfoTaskNode *fft = NULL;
    UfoTaskNode *powerspectrum = NULL;
    GError *error = NULL;

    pm = ufo_plugin_manager_new ();

    avg_bg = make_node (pm, "average-kalypso");
    avg_unmod = make_node (pm, "average-kalypso");
    correct = make_node (pm, "correct-kalypso");

    if (params->output != NULL) {
        write = make_node (pm, "write");
        g_object_set (write, "filename", params->output, NULL);
    }
    else {
        write = make_node (pm, "null");
        g_object_set (write, "download", FALSE, NULL);
    }

    graph = UFO_TASK_GRAPH (ufo_task_graph_new ());

    if (params->use_dummy) {
        bg = make_node (pm, "dummy-data");
        mod = make_node (pm, "dummy-data");
        unmod = make_node (pm, "dummy-data");

        g_object_set (bg, "width", 256, "height", params->number, NULL);
        g_object_set (mod, "width", 256, "height", params->number, NULL);
        g_object_set (unmod, "width", 256, "height", params->number, NULL);
    }
    else {
        bg = make_node (pm, "read");
        unmod = make_node (pm, "read");

        set_read_properties (bg, params->background, params->number);
        set_read_properties (unmod, params->unmodulated, params->number);

        if (params->use_dgma) {
            mod = make_node (pm, "direct-gma");

            g_object_set (mod,
                          "number", 1, /* read only one "frame" */
                          "width", 256,
                          "height", params->number,
                          NULL);
        }
        else {
            mod = make_node (pm, "read");
            set_read_properties (mod, params->modulated, params->number);
        }
    }

    ufo_task_graph_connect_nodes (graph, bg, avg_bg);
    ufo_task_graph_connect_nodes (graph, unmod, avg_unmod);
    ufo_task_graph_connect_nodes_full (graph, mod, correct, 0);
    ufo_task_graph_connect_nodes_full (graph, avg_bg, correct, 1);
    ufo_task_graph_connect_nodes_full (graph, avg_unmod, correct, 2);

    previous = correct;

    if (params->lowpass) {
        lowpass = make_node (pm, "moving-average");
        ufo_task_graph_connect_nodes (graph, previous, lowpass);
        previous = lowpass;
    }

    if (params->powerspectrum) {
        fft = make_node (pm, "fft");
        powerspectrum = make_node (pm, "powerspectrum");
        ufo_task_graph_connect_nodes (graph, previous, fft);
        ufo_task_graph_connect_nodes (graph, fft, powerspectrum);
        previous = powerspectrum;
    }

    if (params->time) {
        transpose = make_node (pm, "transpose");
        ufo_task_graph_connect_nodes (graph, previous, transpose);
        previous = transpose;
    }

    if (params->argmax) {
        argmax = make_node (pm, "argmax");
        ufo_task_graph_connect_nodes (graph, previous, argmax);
        previous = argmax;
    }

    ufo_task_graph_connect_nodes (graph, previous, write);

    /* run */
    sched = ufo_scheduler_new ();
    g_object_set (sched, "enable-tracing", params->profile, NULL);
    ufo_base_scheduler_run (sched, graph, &error);

    if (error != NULL) {
        g_print ("Error running: %s\n", error->message);
        return FALSE;
    }

    g_object_get (sched, "time", &run_time, NULL);
    g_print ("Finished in %3.5fs\n", run_time);

    /* cleanup */
    g_object_unref (bg);
    g_object_unref (mod);
    g_object_unref (unmod);
    g_object_unref (avg_bg);
    g_object_unref (avg_unmod);
    g_object_unref (correct);
    g_object_unref (write);

    if (params->lowpass)
        g_object_unref (lowpass);

    if (params->argmax)
        g_object_unref (argmax);

    if (params->time)
        g_object_unref (transpose);

    if (params->powerspectrum) {
        g_object_unref (powerspectrum);
        g_object_unref (fft);
    }

    /* g_object_unref (graph); */
    g_object_unref (sched);
    g_object_unref (pm);

    return TRUE;
}

int
main (int argc, char **argv)
{
    static Parameters params = {
        .background = NULL,
        .unmodulated = NULL,
        .modulated = NULL,
        .output = NULL,
        .number = 256,
        .lowpass = FALSE,
        .argmax = FALSE,
        .time = FALSE,
        .powerspectrum = FALSE,
        .profile = FALSE,
        .use_dummy = FALSE,
    };

    static const GOptionEntry entries[] = {
        { "background",   'b', 0, G_OPTION_ARG_FILENAME, &params.background,    "Background data",  "FILE" },
        { "unmodulated",  'u', 0, G_OPTION_ARG_FILENAME, &params.unmodulated,   "Unmodulated data", "FILE" },
        { "modulated",    'm', 0, G_OPTION_ARG_FILENAME, &params.modulated,     "Modulated data",   "FILE" },
        { "output",       'o', 0, G_OPTION_ARG_FILENAME, &params.output,        "Output data",      "FILE" },
        { "number",       'n', 0, G_OPTION_ARG_INT,      &params.number,        "Number of datasets to analyze", "N" },
        { "lowpass",        0, 0, G_OPTION_ARG_NONE,     &params.lowpass,       "Enable lowpass", NULL },
        { "argmax",         0, 0, G_OPTION_ARG_NONE,     &params.argmax,        "Enable argmax", NULL },
        { "time",           0, 0, G_OPTION_ARG_NONE,     &params.time,          "Enable time domain", NULL },
        { "powerspectrum",  0, 0, G_OPTION_ARG_NONE,     &params.powerspectrum, "Enable powerspectrum", NULL },
        { "profile",        0, 0, G_OPTION_ARG_NONE,     &params.profile,       "Enable profiling", NULL },
        { "use-dgma",       0, 0, G_OPTION_ARG_NONE,     &params.use_dgma,      "Read from FPGA", NULL },
        { "use-dummy",      0, 0, G_OPTION_ARG_NONE,     &params.use_dummy,     "Simulate input data", NULL },
        { NULL },
    };

    GOptionContext *context;
    GError *error = NULL;
    guint last_input;

    context = g_option_context_new (NULL);
    g_option_context_add_main_entries (context, entries, NULL);

    if (!g_option_context_parse (context, &argc, &argv, &error)) {
        g_printerr ("Error parsing options: %s\n", error->message);
        return 1;
    }

    if (params.use_dgma && params.use_dummy) {
        g_printerr ("Error: cannot have both --use-dgma and --use-dummy\n");
        return 1;
    }

    /* we do not want to read modulated file if we use the FPGA */
    last_input = params.use_dgma ? 2 : (params.use_dummy ? 0 : 3);

    /* check that first options are set */
    for (int i = 0; i < last_input; i++) {
        const gchar *path;

        path = *((const char **) entries[i].arg_data);

        if (path == NULL || strlen (path) == 0) {
            g_printerr ("Error: parameter -%c/--%s is required.\n", entries[i].short_name, entries[i].long_name);
            return 1;
        }
    }

    if (!run (&params))
        return 1;

    g_option_context_free (context);
    return 0;
}
