find_package(PkgConfig REQUIRED)

pkg_check_modules(UFO ufo>=${UFO_CORE_MINIMUM} REQUIRED)

include_directories(${UFO_INCLUDE_DIRS})
link_directories(${UFO_LIBRARY_DIRS})

add_definitions("-std=c99 -Wall")
add_executable(kalypso kalypso.c)
target_link_libraries(kalypso ${UFO_LIBRARIES})

install(PROGRAMS
        ${CMAKE_CURRENT_SOURCE_DIR}/kalypso.py
        ${CMAKE_CURRENT_BINARY_DIR}/kalypso
        DESTINATION ${CMAKE_INSTALL_BINDIR})
