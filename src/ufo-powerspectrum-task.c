/*
 * Copyright (C) 2011-2015 Karlsruhe Institute of Technology
 *
 * This file is part of Ufo.
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#include "ufo-powerspectrum-task.h"


struct _UfoPowerspectrumTaskPrivate {
    cl_kernel kernel;
};

static void ufo_task_interface_init (UfoTaskIface *iface);

G_DEFINE_TYPE_WITH_CODE (UfoPowerspectrumTask, ufo_powerspectrum_task, UFO_TYPE_TASK_NODE,
                         G_IMPLEMENT_INTERFACE (UFO_TYPE_TASK,
                                                ufo_task_interface_init))

#define UFO_POWERSPECTRUM_TASK_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE((obj), UFO_TYPE_POWERSPECTRUM_TASK, UfoPowerspectrumTaskPrivate))

UfoNode *
ufo_powerspectrum_task_new (void)
{
    return UFO_NODE (g_object_new (UFO_TYPE_POWERSPECTRUM_TASK, NULL));
}

static void
ufo_powerspectrum_task_setup (UfoTask *task,
                              UfoResources *resources,
                              GError **error)
{
    UfoPowerspectrumTaskPrivate *priv;

    priv = UFO_POWERSPECTRUM_TASK_GET_PRIVATE (task);
    priv->kernel = ufo_resources_get_kernel (resources, "kalypso.cl", "powerspectrum", NULL, error);
}

static void
ufo_powerspectrum_task_get_requisition (UfoTask *task,
                                        UfoBuffer **inputs,
                                        UfoRequisition *requisition)
{
    ufo_buffer_get_requisition (inputs[0], requisition);

    /* complex to real */
    requisition->dims[0] /= 2;
}

static guint
ufo_powerspectrum_task_get_num_inputs (UfoTask *task)
{
    return 1;
}

static guint
ufo_powerspectrum_task_get_num_dimensions (UfoTask *task,
                                            guint input)
{
    return 2;
}

static UfoTaskMode
ufo_powerspectrum_task_get_mode (UfoTask *task)
{
    return UFO_TASK_MODE_PROCESSOR | UFO_TASK_MODE_GPU;
}

static gboolean
ufo_powerspectrum_task_process (UfoTask *task,
                         UfoBuffer **inputs,
                         UfoBuffer *output,
                         UfoRequisition *requisition)
{
    UfoPowerspectrumTaskPrivate *priv;
    UfoGpuNode *node;
    UfoProfiler *profiler;
    UfoRequisition in_req;
    cl_command_queue queue;
    cl_mem in_mem;
    cl_mem out_mem;
    size_t work_size[2];

    priv = UFO_POWERSPECTRUM_TASK_GET_PRIVATE (task);

    profiler = ufo_task_node_get_profiler (UFO_TASK_NODE (task));
    node = UFO_GPU_NODE (ufo_task_node_get_proc_node (UFO_TASK_NODE (task)));
    queue = ufo_gpu_node_get_cmd_queue (node);
    in_mem = ufo_buffer_get_device_array (inputs[0], queue);
    out_mem = ufo_buffer_get_device_array (output, queue);

    ufo_buffer_get_requisition (inputs[0], &in_req);
    work_size[0] = in_req.dims[0] / 2;
    work_size[1] = in_req.dims[1];

    UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (priv->kernel, 0, sizeof (cl_mem), &in_mem));
    UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (priv->kernel, 1, sizeof (cl_mem), &out_mem));
    ufo_profiler_call (profiler, queue, priv->kernel, 2, work_size, NULL);

    return TRUE;
}

static void
ufo_task_interface_init (UfoTaskIface *iface)
{
    iface->setup = ufo_powerspectrum_task_setup;
    iface->get_num_inputs = ufo_powerspectrum_task_get_num_inputs;
    iface->get_num_dimensions = ufo_powerspectrum_task_get_num_dimensions;
    iface->get_mode = ufo_powerspectrum_task_get_mode;
    iface->get_requisition = ufo_powerspectrum_task_get_requisition;
    iface->process = ufo_powerspectrum_task_process;
}

static void
ufo_powerspectrum_task_class_init (UfoPowerspectrumTaskClass *klass)
{
    GObjectClass *oclass = G_OBJECT_CLASS (klass);
    g_type_class_add_private (oclass, sizeof(UfoPowerspectrumTaskPrivate));
}

static void
ufo_powerspectrum_task_init(UfoPowerspectrumTask *self)
{
    self->priv = UFO_POWERSPECTRUM_TASK_GET_PRIVATE(self);
}
