/*
 * Copyright (C) 2011-2013 Karlsruhe Institute of Technology
 *
 * This file is part of Ufo.
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __UFO_MOVING_AVERAGE_TASK_H
#define __UFO_MOVING_AVERAGE_TASK_H

#include <ufo/ufo.h>

G_BEGIN_DECLS

#define UFO_TYPE_MOVING_AVERAGE_TASK             (ufo_moving_average_task_get_type())
#define UFO_MOVING_AVERAGE_TASK(obj)             (G_TYPE_CHECK_INSTANCE_CAST((obj), UFO_TYPE_MOVING_AVERAGE_TASK, UfoMovingAverageTask))
#define UFO_IS_MOVING_AVERAGE_TASK(obj)          (G_TYPE_CHECK_INSTANCE_TYPE((obj), UFO_TYPE_MOVING_AVERAGE_TASK))
#define UFO_MOVING_AVERAGE_TASK_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST((klass), UFO_TYPE_MOVING_AVERAGE_TASK, UfoMovingAverageTaskClass))
#define UFO_IS_MOVING_AVERAGE_TASK_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE((klass), UFO_TYPE_MOVING_AVERAGE_TASK))
#define UFO_MOVING_AVERAGE_TASK_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS((obj), UFO_TYPE_MOVING_AVERAGE_TASK, UfoMovingAverageTaskClass))

typedef struct _UfoMovingAverageTask           UfoMovingAverageTask;
typedef struct _UfoMovingAverageTaskClass      UfoMovingAverageTaskClass;
typedef struct _UfoMovingAverageTaskPrivate    UfoMovingAverageTaskPrivate;

struct _UfoMovingAverageTask {
    UfoTaskNode parent_instance;

    UfoMovingAverageTaskPrivate *priv;
};

struct _UfoMovingAverageTaskClass {
    UfoTaskNodeClass parent_class;
};

UfoNode  *ufo_moving_average_task_new       (void);
GType     ufo_moving_average_task_get_type  (void);

G_END_DECLS

#endif
