kernel void
average (global unsigned short *input, unsigned num_elements, global float *output)
{
    size_t id = get_global_id (0);
    double sum = 0.0;
    unsigned index = id;

    for (unsigned i = 0; i < num_elements; i++) {
        sum += input[index] & 0x3fff;
        index += 256;
    }

    output[id] = (float) (sum / num_elements);
}

kernel void
correct (global unsigned short *input, constant float *background, constant float *unmodulated, global float *output)
{
    size_t idx = get_global_id (0);
    size_t idy = get_global_id (1);
    size_t id = idy * 256 + idx;

    local float b[256];
    local float u[256];

    b[idx] = background[idx];
    u[idx] = unmodulated[idx] - b[idx];

    barrier (CLK_LOCAL_MEM_FENCE);

    output[id] = (((float) (input[id] & 0x3fff)) - b[idx]) / u[idx];
}

kernel void
argmax (global float *input, unsigned num_elements, global float *output)
{
    size_t idx = get_global_id (0);
    float m = 0.0f;
    int index = 0;

    /* shift to reach proper data element */
    idx *= num_elements;

    for (unsigned i = 0; i < num_elements; i++) {
        float d = input[idx + i];

        if (!isnan (d) && !isinf(d) && d > m) {
            m = d;
            index = i;
        }
    }

    output[get_global_id (0)] = (float) index;
}

kernel void
moving_average (global float *input, unsigned num_elements, global float *output)
{
    size_t idx = get_global_id (0);
    int order = 3;
    float m = 0.0f;

    /* shift to reach proper data element */
    idx *= num_elements;

    for (unsigned i = 0; i < order; i++) {
        m += input[idx + i];
        output[idx + i] = 0.0f;
    }

    m /= order;

    for (unsigned i = order; i < num_elements; i++) {
        output[idx + i - 1] = m;

        m += input[idx + i] / order;
        m -= input[idx + i - order] / order;
    }
}

kernel void
powerspectrum (global float *input, global float *output)
{
    size_t row = get_global_size (0) * get_global_id (1);
    size_t idx = get_global_id (0);
    float re, im, r;

    re = input[row + 2 * idx];
    im = input[row + 2 * idx + 1];
    r = sqrt (re * re + im * im);

    output[row + idx] = 10 * log10 (r / get_global_size (0) / 2);
}
