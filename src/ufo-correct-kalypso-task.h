/*
 * Copyright (C) 2011-2013 Karlsruhe Institute of Technology
 *
 * This file is part of Ufo.
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __UFO_CORRECT_KALYPSO_TASK_H
#define __UFO_CORRECT_KALYPSO_TASK_H

#include <ufo/ufo.h>

G_BEGIN_DECLS

#define UFO_TYPE_CORRECT_KALYPSO_TASK             (ufo_correct_kalypso_task_get_type())
#define UFO_CORRECT_KALYPSO_TASK(obj)             (G_TYPE_CHECK_INSTANCE_CAST((obj), UFO_TYPE_CORRECT_KALYPSO_TASK, UfoCorrectKalypsoTask))
#define UFO_IS_CORRECT_KALYPSO_TASK(obj)          (G_TYPE_CHECK_INSTANCE_TYPE((obj), UFO_TYPE_CORRECT_KALYPSO_TASK))
#define UFO_CORRECT_KALYPSO_TASK_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST((klass), UFO_TYPE_CORRECT_KALYPSO_TASK, UfoCorrectKalypsoTaskClass))
#define UFO_IS_CORRECT_KALYPSO_TASK_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE((klass), UFO_TYPE_CORRECT_KALYPSO_TASK))
#define UFO_CORRECT_KALYPSO_TASK_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS((obj), UFO_TYPE_CORRECT_KALYPSO_TASK, UfoCorrectKalypsoTaskClass))

typedef struct _UfoCorrectKalypsoTask           UfoCorrectKalypsoTask;
typedef struct _UfoCorrectKalypsoTaskClass      UfoCorrectKalypsoTaskClass;
typedef struct _UfoCorrectKalypsoTaskPrivate    UfoCorrectKalypsoTaskPrivate;

struct _UfoCorrectKalypsoTask {
    UfoTaskNode parent_instance;

    UfoCorrectKalypsoTaskPrivate *priv;
};

struct _UfoCorrectKalypsoTaskClass {
    UfoTaskNodeClass parent_class;
};

UfoNode  *ufo_correct_kalypso_task_new       (void);
GType     ufo_correct_kalypso_task_get_type  (void);

G_END_DECLS

#endif